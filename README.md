# work1
1.新建一个Maven工程，项目名为：Test+学号后两位(Test12)，请编写一个UserService类，User类(id，name，password三个属性)。UserService类提供一个方法叫printUser，可以输出User类的所有属性。
1、请使用Spring Bean装配方法装配两个类，name要使用本入的姓名进行装配。(10分)
2、使用核心容器获取UserService的对象，并调用printUser方法输出User信息。(10分)
3、使用AspectJ对工程中的printUser方法进行切面入，切点名称为printUserPointCut，在切面中接收前置通知，通知响应函数中打印语句“user information is printing.”并且在控制台输出目标类对象。(10分)
1和3，
![输入图片说明](13.PNG)
![输入图片说明](15.PNG)
2，
![输入图片说明](16.PNG)
运行结果
![输入图片说明](https://images.gitee.com/uploads/images/2022/0615/113339_fe2c19f7_10504807.png "3.PNG")