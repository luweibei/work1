package com.itheima.XmlAdvice;

import org.aspectj.lang.JoinPoint;

public class Xmladvice {
    //前置通知
    public void before(JoinPoint joinPoint){
        System.out.println("user information is printing.");
        System.out.print("目标类是："+joinPoint.getTarget());
        System.out.println("，被织入增强处理的目标方法为："+joinPoint.getSignature().getName());
    }
    //异常通知
    public void afterException(){
        System.out.println("异常通知！");
    }

}
