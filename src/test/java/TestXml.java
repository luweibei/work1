import com.itheima.entity.User;
import com.itheima.service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestXml {
    public static void main(String[] args){
        ApplicationContext context=new
                ClassPathXmlApplicationContext("applicationcontext.xml");
        User user=(User)context.getBean("user");
        UserService userService=(UserService)context.getBean("userservice");
        userService.printUser(user);

    }
}